module.exports = function(source) {
  "use strict";

  let fs = require('fs');

  const absoluteFilePath = this.context,
        filename         = this.resourcePath.replace(absoluteFilePath, ''),
        jsFilename       = absoluteFilePath.replace('views', 'js') + filename.replace('.vue', '.js'),
        lessFilename     = absoluteFilePath.replace('views', 'less') + filename.replace('.vue', '.less');

  this.cacheable(true);

  if (fs.existsSync(jsFilename)) {
    source += '<script src="' + jsFilename + '"></script>\n\n';
  }

  if (fs.existsSync(lessFilename)) {
    source += '<style lang="less" scoped src="' + lessFilename + '"></style>\n\n';
  }

  return source;
};
