import Vue from 'vue';

// Enums
import { ViewId } from '../../../classes/generic/view/ViewId';
import { ViewName } from '../../../classes/generic/view/ViewName';

// Components
import { EditorHook } from '../../views/editor/EditorHook';

// Mixins
import { Editable } from '../editor/mixin/Editable';
import { Model } from '../generic/mixin/Model';

export default Vue.component(ViewName.NUMERIC_VALUE, {
  mixins : [
    Editable,
    Model(ViewId.NUMERIC_VALUE)
  ],
  props  : {
    ellipsis : {
      type    : Boolean,
      default : true
    }
  }
});
