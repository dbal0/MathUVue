import Vue from 'vue';

// Enums
import { ViewId } from '../../../classes/generic/view/ViewId';
import { ViewName } from '../../../classes/generic/view/ViewName';

// Components
import { EditorHook } from '../../views/editor/EditorHook';
import { Vector } from '../../views/matrix/Vector';

// Mixins
import { Editable } from '../editor/mixin/Editable';
import { Model } from '../generic/mixin/Model';

export default Vue.component(ViewName.MATRIX, {
  mixins : [
    Editable,
    Model(ViewId.MATRIX)
  ]
});
