import Vue from 'vue';

// Enums
import { ViewId } from '../../../classes/generic/view/ViewId';
import { ViewName } from '../../../classes/generic/view/ViewName';

// Mixins
import { Editable } from '../editor/mixin/Editable';
import { Model } from '../generic/mixin/Model';

export default Vue.component(ViewName.VECTOR, {
  mixins : [
    Editable,
    Model(ViewId.VECTOR)
  ]
});
