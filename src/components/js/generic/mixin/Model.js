import { ModelProxyFactory } from '../../../../classes/generic/ModelProxyFactory';

export const Model = function(viewId) {
  return {
    props   : {
      model : {
        type    : ModelProxyFactory.getAllowedTypes(viewId),
        default : function() {
          return ModelProxyFactory.getDefaultValue(viewId);
        }
      }
    },
    data    : function() {
      return {
        proxyModel : this.getProxyModel()
      };
    },
    watch   : {
      model : function() {
        this.proxyModel = this.getProxyModel();
      }
    },
    methods : {
      getProxyModel : function() {
        return ModelProxyFactory.proxy(viewId, this.model);
      }
    }
  };
};
