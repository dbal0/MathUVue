// Mixins
import { Model } from '../../generic/mixin/Model';

// Classes
import { Cache } from '../../../../classes/generic/cache/Cache';
import { LazyCache } from '../../../../classes/generic/cache/LazyCache';

export const Cacheable = function(viewId) {
  return {
    mixins  : [
      Model(viewId)
    ],
    props   : {
      cached : {
        type    : Boolean,
        default : true
      }
    },
    created : function() {
      this.updateCache();
    },
    watch   : {
      cached : function() {
        this.updateCache();
      }
    },
    methods : {
      updateCache : function() {
        const cache = this.cached ? new LazyCache(this.proxyModel) : new Cache(this.proxyModel);

        this.proxyModel.cache = cache;
      }
    }
  };
};
