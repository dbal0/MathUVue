// Classes
import { Editor } from '../../../../classes/editor/Editor';

export const Editable = {
  props : {
    editable : {
      type    : Boolean,
      default : false
    },
    editor   : {
      type    : Editor,
      default : function() {
        return Editor.getDefault();
      }
    },
    focused  : {
      type    : Boolean,
      default : false
    }
  }
};
