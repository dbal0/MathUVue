import Vue from 'vue';

// Enums
import { ViewId } from '../../../classes/generic/view/ViewId';
import { ViewName } from '../../../classes/generic/view/ViewName';

// Components
import { MenuItem } from '../../views/editor/menu/MenuItem';

// Mixins
import { Model } from '../generic/mixin/Model';

export default Vue.component(ViewName.EDITOR, {
  mixins  : [
    Model(ViewId.EDITOR)
  ],
  data    : function() {
    return {
      editable : true,
      slotted  : this.$slots.default.length > 0
    };
  },
  methods : {
    onElementEdited : function(element) {
      this.slotted = this.slotted && this.$slots.default.find(
        (node) => node.componentInstance.proxyModel === element.delegate
      ) === undefined;

      this.$emit('element-edited', element);
    }
  }
});
