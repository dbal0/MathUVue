import Vue from 'vue';

// Enums
import { ViewId } from '../../../../classes/generic/view/ViewId';
import { ViewName } from '../../../../classes/generic/view/ViewName';

// Classes
import { Editor } from '../../../../classes/editor/Editor';

// Mixins
import { Model } from "../../generic/mixin/Model";

export default Vue.component(ViewName.MENU_ITEM, {
  mixins   : [
    Model(ViewId.MENU_ITEM)
  ],
  props    : {
    editor    : {
      type    : Editor,
      default : function() {
        return Editor.getDefault();
      }
    },
    collapsed : {
      type    : Boolean,
      default : true
    },
    depth     : {
      type    : Number,
      default : 0
    }
  },
  mounted  : function() {
    this.proxyModel.collapsed = this.collapsed;
  },
  computed : {
    empty      : function() {
      return this.proxyModel.items.length === 0;
    },
    compatible : function() {
      return !this.empty || this.editor.canEdit(this.proxyModel.element);
    }
  },
  watch    : {
    collapsed : function() {
      this.proxyModel.collapsed = this.collapsed;
    }
  },
  methods  : {
    toggle          : function() {
      this.proxyModel.collapsed = !this.proxyModel.collapsed;
    },
    edit            : function() {
      const oldElement = this.editor.edit(this.proxyModel.element.copy());

      this.onElementEdited(oldElement);

      this.editor.reset();
    },
    onElementEdited : function(oldElement) {
      this.$emit('element-edited', oldElement);
    }
  }
});
