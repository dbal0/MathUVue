import Vue from 'vue';

// Enums
import { ViewId } from '../../../classes/generic/view/ViewId';
import { ViewName } from '../../../classes/generic/view/ViewName';

// Mixins
import { Editable } from './mixin/Editable';
import { Model } from '../generic/mixin/Model';

export default Vue.component(ViewName.EDITOR_HOOK, {
  mixins        : [
    Editable,
    Model(ViewId.EDITOR_HOOK)
  ],
  mounted       : function() {
    this.proxyModel.focused  = this.focused;
    this.proxyModel.editable = this.editable;
    this.proxyModel.editor   = this.editor;

    this.proxyModel.editor.focus(this.proxyModel);
  },
  beforeDestroy : function() {
    this.editor.unregister(this.proxyModel);
  },
  computed      : {
    focusable : function() {
      return this.proxyModel.editor !== undefined
        && (
          (this.proxyModel.editor.current === undefined && this.proxyModel === this.proxyModel.editor.first)
          || this.proxyModel === this.proxyModel.editor.current
        );
    }
  },
  watch         : {
    editor               : function() {
      this.proxyModel.editor = this.editor;
    },
    editable             : function() {
      this.proxyModel.editable = this.editable;
    },
    focused              : function() {
      this.proxyModel.focused = this.focused;
    },
    'proxyModel.focused' : function() {
      if (this.proxyModel.focused) {
        this.$refs[`button-${this.proxyModel.id}`].focus();
      }
    }
  },
  methods       : {
    toggleSelection : function() {
      this.proxyModel.selected = !this.proxyModel.selected;

      this.$emit(this.proxyModel.selected ? 'element-selected' : 'element-deselected', this.proxyModel);
    },
    prev            : function() {
      this.proxyModel.editor.prev();
    },
    next            : function() {
      this.proxyModel.editor.next();
    },
    deselect        : function() {
      this.proxyModel.editor.deselect();
    },
    onFocus         : function() {
      this.proxyModel.editor.focus(this.proxyModel);

      this.$emit('element-focused', this.proxyModel);
    },
    onBlur          : function() {
      this.proxyModel.editor.blur();

      this.$emit('element-blurred', this.proxyModel);
    }
  }
});
