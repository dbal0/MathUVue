import Vue from 'vue';

// Enums
import { ViewId } from '../../../classes/generic/view/ViewId';
import { ViewName } from '../../../classes/generic/view/ViewName';

// Components
import { BinaryOperatorView } from '../../views/operator/BinaryOperator';

// Mixins
import { BinaryOperator } from './mixin/BinaryOperator';

export default Vue.component(ViewName.EQUALITY, {
  mixins   : [
    BinaryOperator(ViewId.EQUALITY)
  ],
  props    : {
    autosolve : {
      type    : Boolean,
      default : true
    }
  },
  computed : {
    valid : function() {
      this.updateResult();

      return this.proxyModel.isValid();
    }
  },
  methods  : {
    updateResult : function() {
      const result = this.proxyModel.valueOf();

      this.proxyModel.setElement(this.autosolve ? result : result.defaulted(), 1);
    }
  }
});
