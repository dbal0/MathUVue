// Mixins
import { Editable } from '../../editor/mixin/Editable';
import { Cacheable } from '../../generic/mixin/Cacheable';

export const Operator = function(viewId) {
  return {
    mixins : [
      Editable,
      Cacheable(viewId)
    ]
  };
};
