// Mixins
import { Operator } from './Operator';

export const BinaryOperator = function(viewId) {
  return {
    mixins   : [Operator(viewId)],
    computed : {
      operand1 : function() {
        return this.proxyModel.getElement(0);
      },
      operand2 : function() {
        return this.proxyModel.getElement(1);
      }
    }
  };
};
