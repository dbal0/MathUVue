import Vue from 'vue';

// Enums
import { ViewId } from '../../../classes/generic/view/ViewId';
import { ViewName } from '../../../classes/generic/view/ViewName';

// Components
import { EditorHook } from '../../views/editor/EditorHook';

// Mixins
import { BinaryOperator } from './mixin/BinaryOperator';

export default Vue.component(ViewName.BINARY_OPERATOR, {
  mixins : [
    BinaryOperator(ViewId.BINARY_OPERATOR)
  ]
});
