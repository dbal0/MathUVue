import Vue from 'vue';

// Enums
import { ViewId } from '../../../classes/generic/view/ViewId';
import { ViewName } from '../../../classes/generic/view/ViewName';

// Components
import { BinaryOperatorView } from '../../views/operator/BinaryOperator';

// Mixins
import { BinaryOperator } from '../operator/mixin/BinaryOperator';

export default Vue.component(ViewName.ADDITION, {
  mixins : [
    BinaryOperator(ViewId.ADDITION)
  ]
});
