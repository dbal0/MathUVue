// Components
import { EditorView } from '../views/editor/Editor';
import { MatrixView } from '../views/matrix/Matrix';
import { VectorView } from '../views/matrix/Vector';
import { NumericValueView } from '../views/numeric/NumericValue';
import { AdditionView } from '../views/operator/Addition';
import { EqualityView } from '../views/operator/Equality';
import { PlaceHolderView } from '../views/generic/PlaceHolder';

// Classes
import { Editor } from '../../classes/editor/Editor';
import { Addition } from '../../classes/operator/Addition';
import { Equality } from '../../classes/operator/Equality';
import { Matrix } from '../../classes/matrix/Matrix';
import { NumericValue } from '../../classes/numeric/NumericValue';
import { Vector } from '../../classes/matrix/Vector';

const matrix = new Matrix([
  [
    new NumericValue(3),
    new NumericValue(6)
  ],
  [
    new NumericValue(-4),
    new NumericValue(2)
  ]
], 2, 2);

const vector = new Vector([
  new NumericValue(3),
  new NumericValue(5)
]);

const addition = new Addition(vector.transpose(), matrix);

const equality = new Equality(addition);

const editor = new Editor();

export default {
  name : 'app',
  data : function() {
    return {
      editor   : editor,
      equality : equality
    };
  }
};
