import Vue from 'vue';

import AppView from './components/views/App';

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el         : '#app',
  template   : '<app id="app"></app>',
  components : {
    app : AppView
  }
});
