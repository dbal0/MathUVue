// Enums
import { ViewGlyph } from '../../generic/view/ViewGlyph';
import { ViewLabel } from '../../generic/view/ViewLabel';
import { ViewName } from '../../generic/view/ViewName';

// Classes
import { AbstractElement } from '../../generic/AbstractElement';
import { ExtensibleCompositeElement } from '../../generic/ExtensibleCompositeElement';
import { Matrix } from '../../matrix/Matrix';
import { Vector } from '../../matrix/Vector';
import { NumericValue } from '../../numeric/NumericValue';
import { Addition } from '../../operator/Addition';
import { Equality } from '../../operator/Equality';
import { Operator } from '../../operator/Operator';

export class MenuItem extends ExtensibleCompositeElement {
  public static getDefault(): MenuItem {
    if (MenuItem._instance === undefined) {
      MenuItem._instance = new MenuItem(
        new AbstractElement(ViewName.PLACEHOLDER, ViewGlyph.DEFAULT, ViewLabel.DEFAULT),
        [
          new MenuItem(
            new AbstractElement(ViewName.PLACEHOLDER, ViewGlyph.DEFAULT, ViewLabel.ELEMENT),
            [
              new MenuItem(new NumericValue()),
              new MenuItem(new Vector()),
              new MenuItem(new Matrix())
            ]
          ),
          new MenuItem(
            new Operator([], ViewName.OPERATOR, ViewGlyph.DEFAULT, ViewLabel.OPERATOR),
            [
              new MenuItem(new Addition()),
              new MenuItem(new Equality())
            ]
          )
        ]
      );

      MenuItem._instance.collapsed = false;
      MenuItem._instance.displayed = false;
    }

    return MenuItem._instance;
  }

  private static _instance: MenuItem;

  constructor(element: AbstractElement = new AbstractElement(),
              items: MenuItem[]        = []) {
    super(
      items,
      ViewName.MENU_ITEM,
      '',
      ''
    );

    this._element = element;
  }

  protected _element: AbstractElement = new AbstractElement();
  private _collapsed: boolean         = true;
  private _displayed: boolean         = true;

  public copy(): MenuItem {
    return new MenuItem(
      this.element,
      this.items.map(function(item: MenuItem): MenuItem {
        return item.copy();
      })
    );
  }

  public defaulted(): MenuItem {
    return new MenuItem();
  }

  public get element(): AbstractElement {
    return this._element;
  }

  public get items(): MenuItem[] {
    return this.elements as MenuItem[];
  }

  public get collapsed(): boolean {
    return this._collapsed;
  }

  public set collapsed(collapsed: boolean) {
    this._collapsed = collapsed;
  }

  public get displayed(): boolean {
    return this._displayed;
  }

  public set displayed(displayed: boolean) {
    this._displayed = displayed;
  }
}
