// Classes
import { ElementDecorator } from '../generic/decorator/ElementDecorator';
import { Editor } from './Editor';

export class EditableElement extends ElementDecorator {
  private _editable: boolean          = true;
  private _editor: Editor | undefined = undefined;
  private _focused: boolean           = false;
  private _selected: boolean          = false;

  get editable(): boolean {
    return this._editable;
  }

  set editable(editable: boolean) {
    if (!this.editable && this.editor !== undefined && this.focused) {
      this.editor.blur(this);
    }

    this._editable = editable;
  }

  get editor(): Editor | undefined {
    return this._editor;
  }

  set editor(editor: Editor | undefined) {
    if (this.editor !== undefined) {
      this.editor.unregister(this);
    }

    this._editor = editor;

    if (editor !== undefined) {
      editor.register(this);
    }
  }

  public get focused(): boolean {
    return this._focused;
  }

  public set focused(focused: boolean) {
    this._focused = focused;
  }

  get selected(): boolean {
    return this._selected;
  }

  set selected(selected: boolean) {
    this._selected = selected;
  }
}
