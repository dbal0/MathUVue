// Enums
import { ViewName } from '../generic/view/ViewName';

// Classes
import { AbstractElement } from '../generic/AbstractElement';
import { ExtensibleCompositeElement } from '../generic/ExtensibleCompositeElement';
import { CaretManager } from './caret/CaretManager';
import { CaretType } from './caret/CaretType';
import { EditableElement } from './EditableElement';
import { MenuItem } from './menu/MenuItem';
import { RegisteringStrategy } from './register/RegisteringStrategy';

// Errors
import { NoSelectedElementError } from '../generic/error/NoSelectedElementError';

export class Editor extends ExtensibleCompositeElement {
  public static getDefault(): Editor {
    if (Editor._instance === undefined) {
      Editor._instance = new Editor();
    }

    return Editor._instance;
  }

  private static _instance: Editor;

  constructor(elements: AbstractElement[] = [],
              menu: MenuItem              = MenuItem.getDefault()) {
    super(
      elements,
      ViewName.EDITOR
    );

    this._menu = menu;

    this._caretManager        = new CaretManager(this._hookedElements);
    this._registeringStrategy = new RegisteringStrategy(this._hookedElements, this);
  }

  protected _caretManager: CaretManager;
  protected _hookedElements: EditableElement[] = [];
  protected _menu: MenuItem;
  protected _registeringStrategy: RegisteringStrategy;

  public get menu(): MenuItem {
    return this._menu;
  }

  public register(element: EditableElement) {
    if (element.parent === undefined) {
      this.push(element.delegate);
    }

    this._registeringStrategy.register(element);
  }

  public unregister(element: EditableElement) {
    if (element.parent === this) {
      this.remove(element.delegate);
    }

    this._registeringStrategy.unregister(element);
  }

  public edit(newElement: AbstractElement): EditableElement {
    if (this.selected === undefined) {
      throw new NoSelectedElementError(this);
    } else if (this.selected.parent !== undefined) {
      this.selected.parent.replace(this.selected.delegate, newElement);
    }

    return this.selected;
  }

  public next(): EditableElement | undefined {
    return this._caretManager.next(CaretType.RIGHT);
  }

  public prev(): EditableElement | undefined {
    return this._caretManager.next(CaretType.LEFT);
  }

  public focus(element?: EditableElement): EditableElement | undefined {
    return this._caretManager.next(CaretType.AUTO, element);
  }

  public blur(element?: EditableElement) {
    if (element === undefined) {
      this._caretManager.blur();
    } else {
      element.focused = false;
    }
  }

  public get current(): EditableElement | undefined {
    return this._caretManager.current;
  }

  public get focused(): EditableElement | undefined {
    return this._caretManager.current === undefined || !this._caretManager.current.focused
      ? undefined
      : this._caretManager.current;
  }

  public get(index: number): EditableElement | undefined {
    return this._caretManager.get(index);
  }

  public get first(): EditableElement | undefined {
    return this._caretManager.first;
  }

  public get last(): EditableElement | undefined {
    return this._caretManager.last;
  }

  public deselect(element ?: EditableElement) {
    if (element === undefined) {
      this._hookedElements.forEach(function(ownElement: EditableElement) {
        ownElement.selected = false;
      });
    } else {
      element.selected = false;
    }
  }

  public canEdit(element: AbstractElement): boolean {
    return this.selected !== undefined
      && this.selected.parent !== undefined
      && this.selected.parent.accept(element, this.selected.parent.getIndex(this.selected.delegate));
  }

  public reset() {
    this._caretManager.reset();
    this._registeringStrategy.reset();
  }

  private get selected(): EditableElement | undefined {
    return this._hookedElements.find((element: EditableElement) => element.selected);
  }
}
