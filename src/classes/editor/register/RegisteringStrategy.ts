// Classes
import { EditableElement } from '../EditableElement';
import { Editor } from '../Editor';

export class RegisteringStrategy {
  constructor(elements: EditableElement[] = [], editor: Editor = Editor.getDefault()) {
    this._elements = elements;

    this._editor = editor;
  }

  protected _elements: EditableElement[];
  protected _editor: Editor;
  protected _editingIndex: number = -1;

  public register(element: EditableElement) {
    if (this._editingIndex === -1) {
      const elementIndex = this._elements.indexOf(element);

      elementIndex < 0 ? this._elements.push(element) : this._elements.splice(elementIndex, 0, element);
    } else {
      this._elements.splice(this._editingIndex, 0, element);

      if (this._isParentRegistered(element)) {
        this._editingIndex = -1;
      } else {
        ++this._editingIndex;
      }
    }
  }

  public unregister(element: EditableElement) {
    this._editingIndex = this._elements.indexOf(element);

    this._elements.splice(this._editingIndex, 1);
  }

  public reset() {
    this._editingIndex = -1;
  }

  private _isParentRegistered(element: EditableElement): boolean {
    return element.parent === this._editor
      || this._elements.find(
        (editableElement) => editableElement.delegate === element.parent
      ) !== undefined;
  }
}
