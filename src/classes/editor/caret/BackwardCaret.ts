import { EditableElement } from '../EditableElement';
import { Caret } from './Caret';
import { CaretType } from './CaretType';

export class BackwardCaret extends Caret {
  constructor(elements: EditableElement[]) {
    super(elements);

    this._position = elements.length;
    this._type     = CaretType.LEFT;
  }

  protected* _iterate(elements: EditableElement[]): IterableIterator<EditableElement> {
    while (this._position >= 0) {
      const previousEditableIndex = elements.slice(0, Math.max(0, this._position)).reverse().findIndex(
        function(element: EditableElement) {
          return element.editable;
        }
      );

      if (previousEditableIndex >= 0) {
        this._position = this._position - 1 - previousEditableIndex;
      }

      yield elements[this._position];
    }
  }
}
