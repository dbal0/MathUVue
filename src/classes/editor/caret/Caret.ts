// Enums
import { ViewName } from '../../generic/view/ViewName';

// Classes
import { EditableElement } from '../EditableElement';
import { CaretType } from './CaretType';

export class Caret {
  private static _focusable(element: EditableElement) {
    return element.view === ViewName.PLACEHOLDER;
  }

  constructor(elements: EditableElement[]) {
    this._type      = CaretType.AUTO;
    this._position  = 0;
    this._generator = this._iterate(elements);
  }

  protected _done: boolean = false;
  protected _position: number;
  protected _type: CaretType;

  private _generator: IterableIterator<EditableElement>;

  public get done() {
    return this._done;
  }

  public get position(): number {
    return this._position;
  }

  public set position(position: number) {
    this._position = position;
  }

  public get type(): CaretType {
    return this._type;
  }

  public next(): EditableElement | undefined {
    const next = this._generator.next();

    this._done = next.done;

    return next.value;
  }

  protected * _iterate(elements: EditableElement[]): IterableIterator<EditableElement> {
    for (this._position = 0; this._position < elements.length; ++this._position) {
      const element = elements[this._position];

      if (element.editable && Caret._focusable(element)) {
        yield element;
      }
    }
  }
}
