import { EditableElement } from '../EditableElement';
import { BackwardCaret } from './BackwardCaret';
import { Caret } from './Caret';
import { CaretType } from './CaretType';
import { ForwardCaret } from './ForwardCaret';

export class CaretManager {
  constructor(elements: EditableElement[] = []) {
    this.elements = elements;
  }

  protected _current: EditableElement | undefined;
  protected _elements: EditableElement[] = [];
  protected _carets: Caret[]             = [];

  public set elements(elements: EditableElement[]) {
    this.reset();

    this._elements = elements;
  }

  public get current(): EditableElement | undefined {
    return this._current;
  }

  public next(caretType: CaretType, element?: EditableElement): EditableElement | undefined {
    const caret: Caret = this._findCaret(caretType);

    this.blur();

    if (element === undefined) {
      this._current = caret.next();
      this._synchronizeWith(caret);
    } else {
      this._current = element;
      this._synchronize(this._elements.indexOf(element));
    }

    this.focus();

    return this._current;
  }

  public done(caretType: CaretType) {
    const foundCaret: Caret | undefined = this._carets.find(function(caret: Caret) {
      return caret.type === caretType;
    });

    return foundCaret !== undefined && foundCaret.done;
  }

  public focus() {
    if (this._current !== undefined) {
      this._current.focused = true;
    }
  }

  public blur() {
    if (this._current !== undefined) {
      this._current.focused = false;
    }
  }

  public get(index: number): EditableElement | undefined {
    return this._elements[index];
  }

  public get first(): EditableElement | undefined {
    return this.get(0);
  }

  public get last(): EditableElement | undefined {
    return this.get(this._elements.length - 1);
  }

  public reset() {
    if (this._current !== undefined) {
      this._current.focused = false;
    }

    this._current = undefined;

    this._carets = [];
  }

  private _synchronize(position: number = 0) {
    this._carets.forEach(function(caret: Caret) {
      caret.position = position;
    });
  }

  private _synchronizeWith(caret: Caret) {
    this._carets.forEach(function(ownCaret: Caret) {
      if (caret !== ownCaret) {
        ownCaret.position = caret.position;
      }
    });
  }

  private _addCaret(caret: Caret) {
    if (this._carets.length > 0) {
      caret.position = this._carets[0].position;
    }

    this._carets.push(caret);
  }

  private _findCaret(caretType: CaretType): Caret {
    const foundCaret: Caret | undefined = this._carets.find(function(caret: Caret) {
      return caret.type === caretType;
    });

    return foundCaret === undefined ? this._createCaret(caretType) : foundCaret;
  }

  private _createCaret(caretType: CaretType): Caret {
    let caret: Caret;

    switch (caretType) {
      case CaretType.LEFT:
        caret = new BackwardCaret(this._elements);
        break;
      case CaretType.RIGHT:
        caret = new ForwardCaret(this._elements);
        break;
      case CaretType.AUTO:
      default:
        caret = new Caret(this._elements);
        break;
    }

    this._addCaret(caret);

    return caret;
  }
}
