import { EditableElement } from '../EditableElement';
import { Caret } from './Caret';
import { CaretType } from './CaretType';

export class ForwardCaret extends Caret {
  constructor(elements: EditableElement[]) {
    super(elements);

    this._position = -1;
    this._type     = CaretType.RIGHT;
  }

  protected* _iterate(elements: EditableElement[]): IterableIterator<EditableElement> {
    while (this._position < elements.length) {
      const nextEditableIndex = elements.slice(this._position + 1).findIndex(
        function(element) {
          return element.editable;
        }
      );

      if (nextEditableIndex >= 0) {
        this._position = this._position + 1 + nextEditableIndex;
      }

      yield elements[this._position];
    }
  }
}
