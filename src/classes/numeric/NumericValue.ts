// Enums
import { ViewGlyph } from '../generic/view/ViewGlyph';
import { ViewLabel } from '../generic/view/ViewLabel';
import { ViewName } from '../generic/view/ViewName';

// Classes
import { AbstractElement } from '../generic/AbstractElement';
import { BinaryOperatorVisitor } from '../operator/visitors/BinaryOperatorVisitor';

export class NumericValue extends AbstractElement {
  constructor(value: number = 0) {
    super(
      ViewName.NUMERIC_VALUE,
      ViewGlyph.NUMERIC_VALUE,
      ViewLabel.NUMERIC_VALUE
    );

    this._value = value;
  }

  private readonly _value: number;

  public get value(): number {
    return this._value;
  }

  public copy(): NumericValue {
    return new NumericValue(this._value);
  }

  public apply(operatorVisitor: BinaryOperatorVisitor, operand: AbstractElement): any {
    return operatorVisitor.visitNumericValue(this, operand);
  }
}
