// Enums
import { ViewGlyph } from '../generic/view/ViewGlyph';
import { ViewLabel } from '../generic/view/ViewLabel';
import { ViewName } from '../generic/view/ViewName';

// Classes
import { AbstractElement } from '../generic/AbstractElement';
import { ExtensibleCompositeElement } from '../generic/ExtensibleCompositeElement';
import { NumericValue } from '../numeric/NumericValue';
import { BinaryOperatorVisitor } from '../operator/visitors/BinaryOperatorVisitor';
import { Matrix } from './Matrix';

export type VectorElement = AbstractElement | number;

export class Vector extends ExtensibleCompositeElement {
  protected static _proxyElements(elements: VectorElement[]): AbstractElement[] {
    return elements.map(function(element: VectorElement): AbstractElement {
      return element instanceof AbstractElement ? element : new NumericValue(element);
    });
  }

  constructor(elements: VectorElement[] = [],
              length?: number,
              view: string              = ViewName.VECTOR,
              glyph: string             = ViewGlyph.VECTOR,
              label: string             = ViewLabel.VECTOR) {
    super(
      Vector._proxyElements(elements),
      view,
      glyph,
      label
    );

    this.length = length === undefined ? Math.max(1, elements.length) : length;
  }

  public copy(): Vector {
    return new Vector(this.elements.map(function(element: AbstractElement): AbstractElement {
      return element.copy();
    }));
  }

  public defaulted(): Vector {
    return new Vector([], this.length);
  }

  public transpose(): Matrix {
    return new Matrix(this.elements.map(function(element: AbstractElement): Vector {
      return new Vector([element]);
    }));
  }

  public apply(operatorVisitor: BinaryOperatorVisitor, operand: AbstractElement): any {
    return operatorVisitor.visitVector(this, operand);
  }
}
