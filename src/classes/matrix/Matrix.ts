// Enums
import { ViewGlyph } from '../generic/view/ViewGlyph';
import { ViewId } from '../generic/view/ViewId';
import { ViewLabel } from '../generic/view/ViewLabel';
import { ViewName } from '../generic/view/ViewName';

// Classes
import { AbstractElement } from '../generic/AbstractElement';
import { ModelProxyFactory } from '../generic/ModelProxyFactory';
import { BinaryOperatorVisitor } from '../operator/visitors/BinaryOperatorVisitor';
import { Vector } from './Vector';

// Types
import { VectorElement } from './Vector';

export type MatrixElement = Vector | VectorElement[];

export class Matrix extends Vector {
  constructor(vectors: MatrixElement[] = [],
              nbCols?: number,
              nbRows?: number) {
    super(
      vectors.map(ModelProxyFactory.proxy.bind(ModelProxyFactory, ViewId.VECTOR)),
      nbCols,
      ViewName.MATRIX,
      ViewGlyph.MATRIX,
      ViewLabel.MATRIX
    );

    this.nbRows = nbRows === undefined ? Math.max(1, this._getMaxVectorSize()) : nbRows;
  }

  private _nbRows: number = 0;

  public get elements(): Vector[] {
    return this._elements as Vector[];
  }

  public get nbRows(): number {
    return this._nbRows;
  }

  public set nbRows(nbRows: number) {
    for (const vector of this.elements) {
      vector.length = nbRows;
    }

    this._nbRows = nbRows;
  }

  public get nbCols(): number {
    return this.length;
  }

  public set nbCols(nbCols: number) {
    this.length = nbCols;
  }

  public copy(): Matrix {
    return new Matrix(this.elements.map(function(element: Vector): Vector {
      return element.copy();
    }));
  }

  public defaulted(): Matrix {
    return new Matrix([], this.nbCols, this.nbRows);
  }

  public getElement(columnIndex: number, rowIndex?: number): AbstractElement {
    const element: Vector = super.getElement(columnIndex) as Vector;

    return rowIndex === undefined ? element : element.getElement(rowIndex);
  }

  public transpose(): Matrix {
    const transposedVectors: Vector[] = [];

    for (let rowIndex = 0; rowIndex < this.nbRows; ++rowIndex) {
      transposedVectors.push(
        new Vector(this.elements.map(
          function(element: Vector) {
            return element.getElement(rowIndex);
          })
        )
      );
    }

    return new Matrix(transposedVectors);
  }

  public apply(operatorVisitor: BinaryOperatorVisitor, operand: AbstractElement): any {
    return operatorVisitor.visitMatrix(this, operand);
  }

  public accept(element: AbstractElement, index: number): boolean {
    return element instanceof Vector && !(element instanceof Matrix);
  }

  protected _getDefaultElement(): Vector {
    return new Vector([], this.nbRows);
  }

  protected _setElement(newElement: Vector, index: number): AbstractElement | undefined {
    newElement.length = this.nbRows;

    return super._setElement(newElement, index);
  }

  private _getMaxVectorSize(): number {
    return Math.max(...this.elements.map(function(vector: Vector): number {
      return vector.length;
    }));
  }
}
