// Enums
import { ViewGlyph } from './view/ViewGlyph';
import { ViewLabel } from './view/ViewLabel';
import { ViewName } from './view/ViewName';

// Classes
import { BinaryOperatorVisitor } from '../operator/visitors/BinaryOperatorVisitor';
import { CompositeElement } from './CompositeElement';

export class AbstractElement {
  private static * _generateId(): IterableIterator<number> {
    let index = 0;

    while (true) {
      yield ++index;
    }
  }

  private static _idGenerator: Generator = AbstractElement._generateId();

  constructor(view: string  = ViewName.PLACEHOLDER,
              glyph: string = ViewGlyph.PLACEHOLDER,
              label: string = ViewLabel.PLACEHOLDER) {
    this._view  = view;
    this._glyph = glyph;
    this._label = label;

    this._id = AbstractElement._idGenerator.next().value;
  }

  protected _parent: CompositeElement | undefined = undefined;
  protected _root: AbstractElement                = this;
  protected _view: string                         = '';
  protected _glyph: string                        = '';
  protected _label: string                        = '';

  private readonly _id: number;

  public get id(): number {
    return this._id;
  }

  public get view(): string {
    return this._view;
  }

  public get glyph(): string {
    return this._glyph;
  }

  public get label(): string {
    return this._label;
  }

  public copy(): AbstractElement {
    return new AbstractElement();
  }

  public defaulted(): AbstractElement {
    return new AbstractElement();
  }

  public valueOf(): AbstractElement {
    return this;
  };

  public get root(): AbstractElement {
    return this._root;
  }

  public get parent(): CompositeElement | undefined {
    return this._parent;
  }

  public set parent(parent: CompositeElement | undefined) {
    this._parent = parent;

    this._root = parent === undefined ? this : parent.root;
  }

  public apply(operatorVisitor: BinaryOperatorVisitor, operand: AbstractElement): any {
    return operatorVisitor.visitAny(this, operand);
  }

  public * [Symbol.iterator](): IterableIterator<AbstractElement> {
    yield this;
  }
}
