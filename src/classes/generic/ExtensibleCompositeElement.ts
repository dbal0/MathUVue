// Classes
import { AbstractElement } from './AbstractElement';
import { CompositeElement } from './CompositeElement';

export class ExtensibleCompositeElement extends CompositeElement {
  public get length() {
    return super.length;
  }

  public set length(length: number) {
    if (length < this.length) {
      this.splice(length);
    } else {
      this._fill(length);
    }
  }

  public setElement(newElement: AbstractElement, index: number): AbstractElement | undefined {
    return this._setElement(newElement, index);
  }

  public push(element: AbstractElement) {
    super._push(element);
  }

  public splice(start: number,
                deleteCount: number = this.elements.length - start,
                ...elements: AbstractElement[]): AbstractElement[] {
    return super._splice(start, deleteCount, ...elements);
  }

  protected _fill(length: number) {
    for (let i: number = this.elements.length; i < length; ++i) {
      this._push(this._getDefaultElement());
    }
  }
}
