// Enums
import { ViewGlyph } from './view/ViewGlyph';
import { ViewLabel } from './view/ViewLabel';
import { ViewName } from './view/ViewName';

// Classes
import { AbstractElement } from './AbstractElement';
import { Cache } from './cache/Cache';
import { LazyCache } from './cache/LazyCache';

// Errors
import { IncompatibleValueError } from './error/IncompatibleValueError';

export class CompositeElement extends AbstractElement {
  constructor(elements: AbstractElement[] = [],
              view: string                = ViewName.PLACEHOLDER,
              glyph: string               = ViewGlyph.PLACEHOLDER,
              label: string               = ViewLabel.PLACEHOLDER) {
    super(
      view,
      glyph,
      label
    );

    elements.forEach(this._push, this);

    this._cache = new LazyCache(this);
  }

  protected _cache: Cache;
  protected _elements: AbstractElement[] = [];

  public get elements(): AbstractElement[] {
    return this._elements;
  }

  public copy(): CompositeElement {
    return new CompositeElement(this.elements.map(function(element: AbstractElement): AbstractElement {
      return element.copy();
    }));
  }

  public defaulted(): CompositeElement {
    return new CompositeElement([]);
  }

  public * [Symbol.iterator](): IterableIterator<AbstractElement> {
    for (const element of this._elements) {
      if (element instanceof CompositeElement) {
        yield * element;
      }

      yield element;
    }
  }

  public getElement(index: number): AbstractElement {
    return this.elements[index];
  }

  public setElement(newElement: AbstractElement, index: number): AbstractElement | undefined {
    return this._setElement(newElement, Math.min(index, this.length));
  }

  public remove(element: AbstractElement): AbstractElement | undefined {
    const index: number = this.getIndex(element);

    return index < 0 ? undefined : this._splice(index, 1).pop();
  }

  public replace(oldElement: AbstractElement, newElement: AbstractElement): AbstractElement | undefined {
    const index: number = this.getIndex(oldElement);

    if (index >= 0) {
      this.setElement(newElement, index);
    }

    return index === null ? undefined : oldElement;
  }

  public getIndex(element: AbstractElement): number {
    return this.elements.findIndex(function(ownElement: AbstractElement) {
      return element.id === ownElement.id;
    });
  }

  public get parent(): CompositeElement | undefined {
    return super.parent;
  }

  public set parent(parent: CompositeElement | undefined) {
    super.parent = parent;

    for (const element of this._elements) {
      element.parent = this;
    }
  }

  public get length(): number {
    return this.elements.length;
  }

  public set cache(cache: Cache) {
    cache.put(this._cache.result);

    this._cache = cache;
  }

  public invalidateCache() {
    this._cache.invalidate();

    if (this.parent !== undefined) {
      this.parent.invalidateCache();
    }
  }

  public valueOf(): AbstractElement {
    let result: AbstractElement | undefined = this._cache.get();

    if (result === undefined) {
      result = this._valueOf();

      this._cache.put(result);
    }

    return result;
  };

  public accept(element: AbstractElement, index: number): boolean {
    return true;
  }

  protected _valueOf(): AbstractElement {
    return super.valueOf();
  }

  protected _setElement(newElement: AbstractElement, index: number): AbstractElement | undefined {
    let replacedElement: AbstractElement | undefined;

    if (this.accept(newElement, index)) {
      replacedElement = this._splice(index, 1, newElement).pop();

      this.invalidateCache();
    } else {
      throw new IncompatibleValueError(this, newElement, index);
    }

    return replacedElement;
  }

  protected _push(element: AbstractElement) {
    this.elements.push(element);

    element.parent = this;
  }

  protected _splice(start: number,
                    deleteCount: number = this.length - start,
                    ...elements: AbstractElement[]): AbstractElement[] {
    const removedElements = this.elements.splice(start, deleteCount, ...elements);

    removedElements.forEach(function(removedElement: AbstractElement) {
      removedElement.parent = undefined;
    });

    for (const element of elements) {
      element.parent = this;
    }

    return removedElements;
  }

  protected _getDefaultElement(): AbstractElement {
    return new AbstractElement();
  }
}
