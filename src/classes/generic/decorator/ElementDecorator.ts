// Classes
import { AbstractElement } from '../AbstractElement';
import { CompositeElement } from '../CompositeElement';

export class ElementDecorator extends AbstractElement {
  constructor(element: AbstractElement = new AbstractElement()) {
    super();

    this._delegate = element;
  }

  protected _delegate: AbstractElement;

  public get delegate(): AbstractElement {
    return this._delegate;
  }

  public get id(): number {
    return this._delegate.id;
  }

  public get view(): string {
    return this._delegate.view;
  }

  public get glyph(): string {
    return this._delegate.glyph;
  }

  public get label(): string {
    return this._delegate.label;
  }

  public copy(): AbstractElement {
    return this._delegate.copy();
  }

  public defaulted(): AbstractElement {
    return this._delegate.defaulted();
  }

  public valueOf(): AbstractElement {
    return this._delegate.valueOf();
  };

  public get root(): AbstractElement {
    return this._delegate.root;
  }

  public get parent(): CompositeElement | undefined {
    return this._delegate.parent;
  }

  public set parent(parent: CompositeElement | undefined) {
    this._delegate.parent = parent;
  }

  public * [Symbol.iterator](): IterableIterator<AbstractElement> {
    yield * this._delegate;
  }
}
