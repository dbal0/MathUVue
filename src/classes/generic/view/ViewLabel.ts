// Enums
import { ViewId } from './ViewId';

/* tslint:disable:variable-name */
export const ViewLabel = Object.freeze({
  DEFAULT                  : '',
  // Elements
  [ViewId.ELEMENT]         : 'Element',
  [ViewId.MATRIX]          : 'Matrix',
  [ViewId.NUMERIC_VALUE]   : 'Numeric value',
  [ViewId.PLACEHOLDER]     : 'Placeholder',
  [ViewId.VECTOR]          : 'Vector',
  // Operators
  [ViewId.ADDITION]        : 'Addition',
  [ViewId.BINARY_OPERATOR] : 'Binary operator',
  [ViewId.DIVISION]        : 'Division',
  [ViewId.EQUALITY]        : 'Equality',
  [ViewId.MULTIPLICATION]  : 'Multiplication',
  [ViewId.OPERATOR]        : 'Operator',
  [ViewId.SUBSTRACTION]    : 'Substraction',
  // Editor
  [ViewId.EDITOR]          : 'Editor',
  [ViewId.EDITOR_HOOK]     : 'Editor hook',
  [ViewId.MENU_ITEM]       : ''
});
