// Enums
import { ViewId } from './ViewId';

/* tslint:disable:variable-name */
export const ViewGlyph = Object.freeze({
  DEFAULT                 : '',
  // Elements
  [ViewId.ELEMENT]        : '',
  [ViewId.PLACEHOLDER]    : '',
  [ViewId.MATRIX]         : '[ ]',
  [ViewId.VECTOR]         : '( )',
  [ViewId.NUMERIC_VALUE]  : '#',
  // Operators
  [ViewId.ADDITION]       : '+',
  [ViewId.DIVISION]       : '÷',
  [ViewId.EQUALITY]       : '=',
  [ViewId.MULTIPLICATION] : '×',
  [ViewId.OPERATOR]       : 'ℜ',
  [ViewId.SUBSTRACTION]   : '−'
});
