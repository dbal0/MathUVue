/* tslint:disable:variable-name */
export const ViewId = Object.freeze({
  // Elements
  ELEMENT         : 'ELEMENT',
  MATRIX          : 'MATRIX',
  NUMERIC_VALUE   : 'NUMERIC_VALUE',
  PLACEHOLDER     : 'PLACEHOLDER',
  VECTOR          : 'VECTOR',
  // Operators
  ADDITION        : 'ADDITION',
  BINARY_OPERATOR : 'BINARY_OPERATOR',
  DIVISION        : 'DIVISION',
  EQUALITY        : 'EQUALITY',
  MULTIPLICATION  : 'MULTIPLICATION',
  OPERATOR        : 'OPERATOR',
  SUBSTRACTION    : 'SUBSTRACTION',
  // Editor
  EDITOR          : 'EDITOR',
  EDITOR_HOOK     : 'EDITOR_HOOK',
  MENU_ITEM       : 'MENU_ITEM'
});
