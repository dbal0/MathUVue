// Enums
import { ViewId } from './ViewId';

/* tslint:disable:variable-name */
export const ViewName = Object.freeze({
  // Elements
  [ViewId.MATRIX]          : 'muv-matrix',
  [ViewId.NUMERIC_VALUE]   : 'muv-numeric-value',
  [ViewId.PLACEHOLDER]     : 'muv-placeholder',
  [ViewId.VECTOR]          : 'muv-vector',
  // Operators
  [ViewId.ADDITION]        : 'muv-addition',
  [ViewId.BINARY_OPERATOR] : 'muv-binary-operator',
  [ViewId.DIVISION]        : 'muv-division',
  [ViewId.EQUALITY]        : 'muv-equality',
  [ViewId.MULTIPLICATION]  : 'muv-multiplication',
  [ViewId.OPERATOR]        : 'muv-operator',
  [ViewId.SUBSTRACTION]    : 'muv-substraction',
  // Editor
  [ViewId.EDITOR]          : 'muv-editor',
  [ViewId.EDITOR_HOOK]     : 'muv-editor-hook',
  [ViewId.MENU_ITEM]       : 'muv-menu-item'
});
