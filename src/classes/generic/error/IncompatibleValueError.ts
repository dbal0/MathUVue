// Classes
import { AbstractElement } from '../AbstractElement';

export class IncompatibleValueError extends Error {
  constructor(target: AbstractElement,
              source: AbstractElement,
              index: number) {
    super(`${target} cannot set ${source} at position ${index}.`);
  }
}
