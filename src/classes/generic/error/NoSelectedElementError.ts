// Classes
import { Editor } from '../../editor/Editor';

export class NoSelectedElementError extends Error {
  constructor(editor: Editor) {
    super(`No selected element to edit in ${editor}.`);
  }
}
