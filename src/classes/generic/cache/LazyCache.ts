// Classes
import { AbstractElement } from '../AbstractElement';
import { Cache } from './Cache';

export class LazyCache extends Cache {
  public get(): AbstractElement | undefined {
    return this._result;
  }
}
