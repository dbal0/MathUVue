// Classes
import { AbstractElement } from '../AbstractElement';

export class Cache {
  constructor(element: AbstractElement) {
    this._element = element;
  }

  protected _element: AbstractElement;
  protected _result: AbstractElement | undefined;

  public get result(): AbstractElement | undefined {
    return this._result;
  }

  public get(): AbstractElement | undefined {
    return undefined;
  }

  public put(result?: AbstractElement) {
    this._result = result;
  }

  public invalidate() {
    this.put();
  }
}
