// Enums
import { ViewId } from './view/ViewId';

// Classes
import { EditableElement } from '../editor/EditableElement';
import { Editor } from '../editor/Editor';
import { MenuItem } from '../editor/menu/MenuItem';
import { Matrix } from '../matrix/Matrix';
import { Vector } from '../matrix/Vector';
import { NumericValue } from '../numeric/NumericValue';
import { Addition } from '../operator/Addition';
import { Equality } from '../operator/Equality';
import { Operator } from '../operator/Operator';
import { AbstractElement } from './AbstractElement';

export class ModelProxyFactory {
  public static proxy(viewName: string,
                      model: any): AbstractElement {
    let proxy: AbstractElement;

    switch (viewName) {
      case ViewId.NUMERIC_VALUE:
        proxy = model instanceof NumericValue ? model : new NumericValue(model);
        break;
      case ViewId.MATRIX:
        proxy = model instanceof Matrix ? model : new Matrix(model);
        break;
      case ViewId.ADDITION:
        proxy = model instanceof Addition ? model : new Addition(model);
        break;
      case ViewId.EQUALITY:
        proxy = model instanceof Equality ? model : new Equality(model);
        break;
      case ViewId.VECTOR:
        proxy = model instanceof Vector ? model : new Vector(model);
        break;
      case ViewId.EDITOR_HOOK:
        proxy = model instanceof EditableElement ? model : new EditableElement(model);
        break;
      case ViewId.MENU_ITEM:
      case ViewId.EDITOR:
      case ViewId.PLACEHOLDER:
      case ViewId.OPERATOR:
      default:
        proxy = model;
        break;
    }

    return proxy;
  }

  public static getAllowedTypes(viewName: string): any[] {
    let allowedTypes: any[];

    switch (viewName) {
      case ViewId.NUMERIC_VALUE:
        allowedTypes = [NumericValue, Number];
        break;
      case ViewId.MATRIX:
        allowedTypes = [Matrix, Array];
        break;
      case ViewId.VECTOR:
        allowedTypes = [Vector, Array];
        break;
      case ViewId.ADDITION:
        allowedTypes = [Addition, Array];
        break;
      case ViewId.EQUALITY:
        allowedTypes = [Equality, AbstractElement];
        break;
      case ViewId.OPERATOR:
        allowedTypes = [Operator];
        break;
      case ViewId.EDITOR:
        allowedTypes = [Editor];
        break;
      case ViewId.EDITOR_HOOK:
        allowedTypes = [AbstractElement];
        break;
      case ViewId.MENU_ITEM:
        allowedTypes = [MenuItem];
        break;
      case ViewId.PLACEHOLDER:
        allowedTypes = [AbstractElement];
        break;
      default:
        allowedTypes = [Object];
        break;
    }

    return allowedTypes;
  }

  public static getDefaultValue(viewName: string): AbstractElement {
    let defaultValue: AbstractElement;

    switch (viewName) {
      case ViewId.NUMERIC_VALUE:
        defaultValue = new NumericValue();
        break;
      case ViewId.MATRIX:
        defaultValue = new Matrix();
        break;
      case  ViewId.VECTOR:
        defaultValue = new Vector();
        break;
      case ViewId.ADDITION:
        defaultValue = new Addition();
        break;
      case ViewId.EQUALITY:
        defaultValue = new Equality();
        break;
      case ViewId.OPERATOR:
        defaultValue = new Operator();
        break;
      case ViewId.EDITOR:
        defaultValue = Editor.getDefault();
        break;
      case ViewId.EDITOR_HOOK:
        defaultValue = new EditableElement(new AbstractElement());
        break;
      case ViewId.MENU_ITEM:
        defaultValue = MenuItem.getDefault();
        break;
      case ViewId.PLACEHOLDER:
      default:
        defaultValue = new AbstractElement();
        break;
    }

    return defaultValue;
  }
}
