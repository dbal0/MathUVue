// Enums
import { ViewGlyph } from '../generic/view/ViewGlyph';
import { ViewLabel } from '../generic/view/ViewLabel';
import { ViewName } from '../generic/view/ViewName';

// Classes
import { AbstractElement } from '../generic/AbstractElement';
import { CompositeElement } from '../generic/CompositeElement';
import { BinaryOperatorVisitor } from './visitors/BinaryOperatorVisitor';

export class Operator extends CompositeElement {
  constructor(operands: AbstractElement[] = [],
              view: string                = ViewName.OPERATOR,
              glyph: string               = ViewGlyph.OPERATOR,
              label: string               = ViewLabel.OPERATOR) {
    super(
      operands,
      view,
      glyph,
      label
    );
  }

  public copy(): Operator {
    return new Operator(
      this.elements.map(function(element: AbstractElement) {
        return element.copy();
      }),
      this.glyph);
  }

  public defaulted(): Operator {
    return new Operator(
      [],
      this.glyph
    );
  }

  public apply(operatorVisitor: BinaryOperatorVisitor, operand: AbstractElement): any {
    return this.valueOf().apply(operatorVisitor, operand);
  }

  protected _valueOf(): AbstractElement {
    return this._getDefaultElement();
  }
}
