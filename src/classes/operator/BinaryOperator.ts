// Enums
import { ViewGlyph } from '../generic/view/ViewGlyph';
import { ViewLabel } from '../generic/view/ViewLabel';
import { ViewName } from '../generic/view/ViewName';

// Classes
import { AbstractElement } from '../generic/AbstractElement';
import { Operator } from './Operator';

export class BinaryOperator extends Operator {
  constructor(operand1: AbstractElement,
              operand2: AbstractElement,
              view: string  = ViewName.BINARY_OPERATOR,
              glyph: string = ViewGlyph.BINARY_OPERATOR,
              label: string = ViewLabel.BINARY_OPERATOR) {
    super(
      [
        operand1,
        operand2
      ],
      view,
      glyph,
      label
    );
  }

  public copy(): BinaryOperator {
    return new BinaryOperator(
      this.getElement(0).copy(),
      this.getElement(1).copy(),
      this.view,
      this.glyph
    );
  }

  public defaulted(): Operator {
    return new BinaryOperator(
      this._getDefaultElement(),
      this._getDefaultElement(),
      this.view,
      this.glyph
    );
  }
}
