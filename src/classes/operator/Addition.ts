// Enums
import { ViewGlyph } from '../generic/view/ViewGlyph';
import { ViewLabel } from '../generic/view/ViewLabel';
import { ViewName } from '../generic/view/ViewName';

// Classes
import { AbstractElement } from '../generic/AbstractElement';
import { NumericValue } from '../numeric/NumericValue';
import { BinaryOperator } from './BinaryOperator';
import { AdditionVisitor } from './visitors/addition/AdditionVisitor';

export type AdditionElement = AbstractElement | number;

export class Addition extends BinaryOperator {
  protected static _proxyOperand(operand: AdditionElement): AbstractElement {
    return operand instanceof AbstractElement ? operand : new NumericValue(operand);
  }

  constructor(operand1: AdditionElement = new AbstractElement(),
              operand2: AdditionElement = new AbstractElement()) {
    super(
      Addition._proxyOperand(operand1),
      Addition._proxyOperand(operand2),
      ViewName.ADDITION,
      ViewGlyph.ADDITION,
      ViewLabel.ADDITION
    );
  }

  public copy(): Addition {
    return new Addition(this.getElement(0).copy(), this.getElement(1).copy());
  }

  public defaulted(): Addition {
    return new Addition(this._getDefaultElement(), this._getDefaultElement());
  }

  protected _valueOf(): AbstractElement {
    return this.getElement(0).apply(AdditionVisitor.getInstance(), this.getElement(1));
  }
}
