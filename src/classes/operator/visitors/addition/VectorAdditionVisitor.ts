// Classes
import { AbstractElement } from '../../../generic/AbstractElement';
import { Matrix } from '../../../matrix/Matrix';
import { Vector } from '../../../matrix/Vector';
import { NumericValue } from '../../../numeric/NumericValue';
import { BinaryOperatorVisitor } from '../BinaryOperatorVisitor';
import { AdditionVisitor } from './AdditionVisitor';
import { MatrixAdditionVisitor } from './MatrixAdditionVisitor';
import { NumericValueAdditionVisitor } from './NumericValueAdditionVisitor';

export class VectorAdditionVisitor extends BinaryOperatorVisitor {
  protected static _vectorAdditionVisitor: VectorAdditionVisitor = new VectorAdditionVisitor();

  public static getInstance(): VectorAdditionVisitor {
    return VectorAdditionVisitor._vectorAdditionVisitor;
  }

  protected static _addElements(vector1: Vector, vector2: Vector): AbstractElement[] {
    return vector1.length < vector2.length ?
      VectorAdditionVisitor._addElements(vector2, vector1) :
      vector1.elements.map(
        function(element: AbstractElement, index: number): AbstractElement {
          return index >= vector2.length ?
            element.copy() :
            element.apply(AdditionVisitor.getInstance(), vector2.getElement(index));
        }
      );
  }

  public visitVector(operand2: Vector, operand1: Vector): Vector {
    return new Vector(VectorAdditionVisitor._addElements(operand1, operand2));
  }

  public visitMatrix(operand2: Matrix, operand1: Vector): Matrix {
    return MatrixAdditionVisitor.getInstance().visitVector(operand1, operand2);
  }

  public visitNumericValue(operand2: NumericValue, operand1: Vector): Vector {
    return new Vector(operand1.elements.map(
      function(element: AbstractElement) {
        return element.apply(NumericValueAdditionVisitor.getInstance(), operand2);
      }
    ), operand1.length);
  }
}
