// Classes
import { Matrix } from '../../../matrix/Matrix';
import { Vector } from '../../../matrix/Vector';
import { NumericValue } from '../../../numeric/NumericValue';
import { VectorAdditionVisitor } from './VectorAdditionVisitor';

export class MatrixAdditionVisitor extends VectorAdditionVisitor {
  protected static _matrixAdditionVisitor: MatrixAdditionVisitor = new MatrixAdditionVisitor();

  public static getInstance(): MatrixAdditionVisitor {
    return MatrixAdditionVisitor._matrixAdditionVisitor;
  }

  protected static _addElements(matrix1: Matrix, matrix2: Vector): Vector[] {
    return super._addElements(matrix1, matrix2) as Vector[];
  }

  public visitVector(operand2: Vector, operand1: Matrix): Matrix {
    const vectors: Vector[] = operand1.elements.slice(1).map(
      function(otherVector: Vector): Vector {
        return otherVector.copy();
      }
    );

    vectors.unshift(VectorAdditionVisitor.getInstance().visitVector(operand2, operand1.getElement(0) as Vector));

    return new Matrix(vectors);
  }

  public visitMatrix(operand2: Matrix, operand1: Matrix): Matrix {
    return new Matrix(MatrixAdditionVisitor._addElements(operand1, operand2));
  }

  public visitNumericValue(operand2: NumericValue, operand1: Matrix): Matrix {
    return new Matrix(operand1.elements.map(function(vector: Vector): Vector {
      return VectorAdditionVisitor.getInstance().visitNumericValue(operand2, vector);
    }), operand1.nbCols, operand1.nbRows);
  }
}
