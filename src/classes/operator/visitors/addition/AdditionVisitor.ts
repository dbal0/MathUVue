// Classes
import { AbstractElement } from '../../../generic/AbstractElement';
import { Matrix } from '../../../matrix/Matrix';
import { Vector } from '../../../matrix/Vector';
import { NumericValue } from '../../../numeric/NumericValue';
import { BinaryOperatorVisitor } from '../BinaryOperatorVisitor';
import { MatrixAdditionVisitor } from './MatrixAdditionVisitor';
import { NumericValueAdditionVisitor } from './NumericValueAdditionVisitor';
import { VectorAdditionVisitor } from './VectorAdditionVisitor';

export class AdditionVisitor extends BinaryOperatorVisitor {
  protected static _additionVisitor: AdditionVisitor = new AdditionVisitor();

  public static getInstance(): AdditionVisitor {
    return AdditionVisitor._additionVisitor;
  }

  protected constructor() {
    super();
  }

  public visitVector(operand1: Vector, operand2: AbstractElement): AbstractElement {
    return operand2.apply(VectorAdditionVisitor.getInstance(), operand1);
  }

  public visitMatrix(operand1: Matrix, operand2: AbstractElement): AbstractElement {
    return operand2.apply(MatrixAdditionVisitor.getInstance(), operand1);
  }

  public visitNumericValue(operand1: NumericValue, operand2: AbstractElement): AbstractElement {
    return operand2.apply(NumericValueAdditionVisitor.getInstance(), operand1);
  }

  public visitAny(operand1: AbstractElement, operand2: AbstractElement): AbstractElement {
    return new AbstractElement();
  }
}
