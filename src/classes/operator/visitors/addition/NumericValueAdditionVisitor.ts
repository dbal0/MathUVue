// Classes
import { Matrix } from '../../../matrix/Matrix';
import { Vector } from '../../../matrix/Vector';
import { NumericValue } from '../../../numeric/NumericValue';
import { BinaryOperatorVisitor } from '../BinaryOperatorVisitor';
import { MatrixAdditionVisitor } from './MatrixAdditionVisitor';
import { VectorAdditionVisitor } from './VectorAdditionVisitor';

export class NumericValueAdditionVisitor extends BinaryOperatorVisitor {
  protected static _numericValueAdditionVisitor: NumericValueAdditionVisitor = new NumericValueAdditionVisitor();

  public static getInstance(): NumericValueAdditionVisitor {
    return NumericValueAdditionVisitor._numericValueAdditionVisitor;
  }

  public visitVector(operand2: Vector, operand1: NumericValue): Vector {
    return VectorAdditionVisitor.getInstance().visitNumericValue(operand1, operand2);
  }

  public visitMatrix(operand2: Matrix, operand1: NumericValue): Matrix {
    return MatrixAdditionVisitor.getInstance().visitNumericValue(operand1, operand2);
  }

  public visitNumericValue(operand2: NumericValue, operand1: NumericValue): NumericValue {
    return new NumericValue(operand1.value + operand2.value);
  }
}
