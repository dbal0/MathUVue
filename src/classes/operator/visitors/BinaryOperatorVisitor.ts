// Classes
import { AbstractElement } from '../../generic/AbstractElement';
import { Matrix } from '../../matrix/Matrix';
import { Vector } from '../../matrix/Vector';
import { NumericValue } from '../../numeric/NumericValue';

export abstract class BinaryOperatorVisitor {
  public abstract visitVector(operand1: Vector, operand2: AbstractElement): any;

  public abstract visitMatrix(operand1: Matrix, operand2: AbstractElement): any;

  public abstract visitNumericValue(operand1: NumericValue, operand2: AbstractElement): any;

  public visitAny(operand1: AbstractElement, operand2: AbstractElement): any {
    return new AbstractElement();
  }
}
