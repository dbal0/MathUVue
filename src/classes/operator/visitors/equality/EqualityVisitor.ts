// Classes
import { AbstractElement } from '../../../generic/AbstractElement';
import { Matrix } from '../../../matrix/Matrix';
import { Vector } from '../../../matrix/Vector';
import { NumericValue } from '../../../numeric/NumericValue';
import { BinaryOperatorVisitor } from '../BinaryOperatorVisitor';
import { MatrixEqualityVisitor } from './MatrixEqualityVisitor';
import { NumericValueEqualityVisitor } from './NumericValueEqualityVisitor';
import { VectorEqualityVisitor } from './VectorEqualityVisitor';

export class EqualityVisitor extends BinaryOperatorVisitor {
  protected static _equalityVisitor: EqualityVisitor = new EqualityVisitor();

  public static getInstance(): EqualityVisitor {
    return EqualityVisitor._equalityVisitor;
  }

  protected constructor() {
    super();
  }

  public visitVector(operand1: Vector, operand2: AbstractElement): boolean {
    return operand2.apply(VectorEqualityVisitor.getInstance(), operand1);
  }

  public visitMatrix(operand1: Matrix, operand2: AbstractElement): boolean {
    return operand2.apply(MatrixEqualityVisitor.getInstance(), operand1);
  }

  public visitNumericValue(operand1: NumericValue, operand2: AbstractElement): boolean {
    return operand2.apply(NumericValueEqualityVisitor.getInstance(), operand1);
  }

  public visitAny(operand1: AbstractElement, operand2: AbstractElement): boolean {
    return operand1.view === operand2.view;
  }
}
