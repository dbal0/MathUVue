// Classes
import { Matrix } from '../../../matrix/Matrix';
import { Vector } from '../../../matrix/Vector';
import { NumericValue } from '../../../numeric/NumericValue';
import { BinaryOperatorVisitor } from '../BinaryOperatorVisitor';

export class NumericValueEqualityVisitor extends BinaryOperatorVisitor {
  protected static _numericValueEqualityVisitor: NumericValueEqualityVisitor = new NumericValueEqualityVisitor();

  public static getInstance(): NumericValueEqualityVisitor {
    return NumericValueEqualityVisitor._numericValueEqualityVisitor;
  }

  public visitVector(operand2: Vector, operand1: NumericValue): boolean {
    return false;
  }

  public visitMatrix(operand2: Matrix, operand1: NumericValue): boolean {
    return false;
  }

  public visitNumericValue(operand2: NumericValue, operand1: NumericValue): boolean {
    return operand1.value === operand2.value;
  }
}
