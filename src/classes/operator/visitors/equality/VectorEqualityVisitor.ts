// Classes
import { AbstractElement } from '../../../generic/AbstractElement';
import { Matrix } from '../../../matrix/Matrix';
import { Vector } from '../../../matrix/Vector';
import { NumericValue } from '../../../numeric/NumericValue';
import { BinaryOperatorVisitor } from '../BinaryOperatorVisitor';
import { EqualityVisitor } from './EqualityVisitor';

export class VectorEqualityVisitor extends BinaryOperatorVisitor {
  protected static _vectorEqualityVisitor: VectorEqualityVisitor = new VectorEqualityVisitor();

  public static getInstance(): VectorEqualityVisitor {
    return VectorEqualityVisitor._vectorEqualityVisitor;
  }

  public visitVector(operand2: Vector, operand1: Vector): boolean {
    return operand1.length === operand2.length &&
      operand1.elements.every(
        function(element: AbstractElement, index: number): boolean {
          return element.apply(EqualityVisitor.getInstance(), operand2.getElement(index));
        }
      );
  }

  public visitMatrix(operand2: Matrix, operand1: Vector): boolean {
    return false;
  }

  public visitNumericValue(operand2: NumericValue, operand1: Vector): boolean {
    return false;
  }
}
