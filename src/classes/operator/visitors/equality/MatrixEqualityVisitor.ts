// Classes
import { Matrix } from '../../../matrix/Matrix';
import { Vector } from '../../../matrix/Vector';
import { NumericValue } from '../../../numeric/NumericValue';
import { VectorEqualityVisitor } from './VectorEqualityVisitor';

export class MatrixEqualityVisitor extends VectorEqualityVisitor {
  protected static _matrixEqualityVisitor: MatrixEqualityVisitor = new MatrixEqualityVisitor();

  public static getInstance(): MatrixEqualityVisitor {
    return MatrixEqualityVisitor._matrixEqualityVisitor;
  }

  public visitVector(operand2: Vector, operand1: Matrix): boolean {
    return false;
  }

  public visitMatrix(operand2: Matrix, operand1: Matrix): boolean {
    return super.visitVector(operand1, operand2);
  }

  public visitNumericValue(operand2: NumericValue, operand1: Matrix): boolean {
    return false;
  }
}
