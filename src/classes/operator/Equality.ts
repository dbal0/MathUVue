// Enums
import { ViewGlyph } from '../generic/view/ViewGlyph';
import { ViewLabel } from '../generic/view/ViewLabel';
import { ViewName } from '../generic/view/ViewName';

// Classes
import { AbstractElement } from '../generic/AbstractElement';
import { BinaryOperator } from './BinaryOperator';
import { EqualityVisitor } from './visitors/equality/EqualityVisitor';

export class Equality extends BinaryOperator {
  constructor(operand1: AbstractElement = new AbstractElement(),
              operand2: AbstractElement = new AbstractElement()) {
    super(
      operand1,
      operand2,
      ViewName.EQUALITY,
      ViewGlyph.EQUALITY,
      ViewLabel.EQUALITY
    );
  }

  public isValid(): boolean {
    return this.valueOf().apply(EqualityVisitor.getInstance(), this.getElement(1));
  }

  public copy(): Equality {
    return new Equality(this.getElement(0).copy());
  }

  public defaulted(): Equality {
    return new Equality(this._getDefaultElement(), this._getDefaultElement());
  }

  protected _valueOf(): AbstractElement {
    // TODO Equality value should be a boolean value
    return this.getElement(0).valueOf();
  }
}
